# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: EUPL-1.2

# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from pathlib import Path

from sphinx.ext import apidoc

sys.path.insert(0, os.path.abspath(".."))

if not os.path.exists("_static"):
    os.makedirs("_static")

# isort: off

import insupdel4stac
import insupdel4stac.analysers as analysers


# isort: on


def generate_apidoc(app):
    appdir = Path(app.__file__).parent
    print("appdir", appdir)
    apidoc.main(
        ["-fMeao", str(api), str(appdir), str(appdir / "migrations" / "*")]
    )


api = Path(os.getcwd() + "/api")


generate_apidoc(insupdel4stac)
generate_apidoc(analysers)


# -- Project information -----------------------------------------------------

project = "INSUPDEL4STAC"
copyright = "2023 Karlsruher Institut für Technologie"
author = "Mostafa Hadizadeh"

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "autodoc_traits",
    "sphinx_copybutton",
    "sphinx.ext.autodoc",
    "sphinxext.opengraph",
    "sphinxext.rediraffe",
    "sphinx.ext.intersphinx",
    "sphinx_design",
    "sphinx.ext.napoleon",
    "sphinx.ext.todo",
    "myst_parser",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.autosummary",
    "sphinx.ext.doctest",
    "sphinx.ext.extlinks",
    "sphinx.ext.coverage",
    "sphinx.ext.viewcode",
    "nbsphinx",
    "sphinxcontrib.jquery",
]
# Notebook integration parameters
nbsphinx_execute = "never"
nbsphinx_prolog = """
{% set docname = env.doc2path(env.docname, base=None) %}

.. hint::

   You can run this notebook in a live session with |Binder| |Helmholtz|.

    * it is important to acknowledge that the utilization of the |Helmholtz| is restricted to individuals who possess the necessary credentials as Helmholtz users. It is highly recommended to utilize the `Python scipy` option when constructing the environment in order to mitigate the occurrence of a `404 Bad request` error.



.. |Binder| image:: https://mybinder.org/badge_logo.svg
   :target: https://mybinder.org/v2/git/https%3A%2F%2Fcodebase.helmholtz.cloud%2Fcat4kit%2Fds2stac%2Finsupdel4stac/main?labpath=%2Fdocs%2Fexamples%2F&urlpath=/lab/tree/docs/{{ docname }}

.. |Helmholtz| image:: https://img.shields.io/badge/launch-Helmholtz-purple.svg
   :target: https://jupyter.desy.de/hub/user-redirect/lab/tree/SHARE/Cat4KIT/DS2STAC/INSUPDEL4STAC/{{ docname }}

"""

nitpicky = True
autosectionlabel_prefix_document = True
root_doc = "index"
source_suffix = [".md", ".rst"]
default_role = "literal"


myst_enable_extensions = ["fieldlist"]
linkcheck_ignore = [
    # we do not check link of the insupdel4stac as the
    # badges might not yet work everywhere. Once insupdel4stac
    # is settled, the following link should be removed
    r"https://.*insupdel4stac"
]


# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

show_authors = True

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.

html_logo = "_static/insupdel4stac-logo.png"
html_favicon = "_static/favicon.ico"
html_static_path = ["_static"]
html_css_files = ["custom.css"]

# pydata_sphinx_theme reference:
# https://pydata-sphinx-theme.readthedocs.io/en/latest/
html_theme = "pydata_sphinx_theme"
html_theme_options = {
    "logo": {
        "image_light": "_static/insupdel4staclight.png",
        "image_dark": "https://codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac/-/raw/development/docs/_static/insupdel4stacdark.svg",  # noqa
    },
    "use_edit_page_button": True,
    "footer_start": ["footer"],
    "footer_end": [
        "sphinx-version",
        "theme-version",
        "last-updated",
    ],
    "gitlab_url": "https://codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac",
    # "external_links": [
    #     {"name": "TDS2STAC", "url": "https://tds2stac.readthedocs.io/"},
    #     {"name": "INTAKE2STAC", "url": "https://intake2stac.readthedocs.io/"}, # noqa
    #     {"name": "STA2STAC", "url": "https://sta2stac.readthedocs.io/"},
    #     {
    #         "name": "INSUPDEL4STAC",
    #         "url": "https://insupdel4stac.readthedocs.io/",
    #     },
    # ],
    "icon_links": [
        {
            "name": "DS2STAC",
            "url": "https://ds2stac.readthedocs.io",
            "icon": "_static/ds2stac-logo-light.png",
            "type": "local",
            # Add additional attributes to the href link.
            "attributes": {
                "target": "_blank",
                "rel": "noopener me",
                "class": "nav-link custom-fancy-css",
            },
        },
        {
            "name": "Cat4KIT",
            "url": "https://cat4kit.readthedocs.io",
            "icon": "_static/cat4kitlogo.png",
            "type": "local",
            # Add additional attributes to the href link.
            "attributes": {
                "target": "_blank",
                "rel": "noopener me",
                "class": "nav-link custom-fancy-css",
            },
        },
    ],
}

html_context = {
    "edit_page_url_template": "https://{{ codebase_url }}/\
{{ codebase_user }}/{{ codebase_repo }}/blob/\
{{ codebase_version }}/{{ doc_path }}{{ file_name }}",
    "codebase_url": "codebase.helmholtz.cloud",
    "codebase_user": "cat4kit/ds2stac",
    "codebase_repo": "insupdel4stac",
    "codebase_version": "main",
    "doc_path": "docs",
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
intersphinx_mapping = {
    "python": ("https://docs.python.org/3/", None),
}
