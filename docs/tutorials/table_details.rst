..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0

.. _table-details:
==================================================
How to make `table_details` argument
==================================================

This table is used to restrict the options available for selecting STAC-Collections and STAC-Items.
The data structure consists of a list of dictionaries, with each dictionary representing a distinct category of choice. There are three distinct keys in each dictionary:

- ``table``: The type of specification between item and collection.
- ``collection-id``: This key refers to the specification of the collection. It applies to both scenarios where we define `table` as either a `collection` or a `item`. The data type can be either a string or a list of strings. To define it, we can either use the precise identification of the collection or a shortened representation of the collection IDs, where the remaining part is indicated by an asterisk (*).
- ``item-id``: The specification of the item and it functions in scenarios when we designate a `table` as a `item`. The input might be either a string or a list of strings. To define it, we can either use the precise id of the item or a shortened version of the item IDs, with the remaining part indicated by an asterisk (*).

Below, we shall discuss it using examples.

In this example, our objective is to choose all the items from the collection identified as `sentinel-s2-l2a-cogs`.

.. code-block:: javascript

    {
        "table": "collection",
        "colleciton-id": "sentinel-s2-l2a-cogs",
    }

In this example, our objective is to choose all the objects in the collection whose id includes the term `sentinel-s2-l2a`.

.. code-block:: javascript

    {
        "table": "collection",
        "colleciton-id": "sentinel-s2-l2a*",
    }

The subsequent illustration is identical to the preceding one, with the objective of choosing all the elements inside the list that encompass 'sentinel-s2-l2a' in their identification and possess 'sentinel-s2-l2a-cogs' equivalent to the collection id.

.. code-block:: javascript

    {
        "table": "collection",
        "colleciton-id": ["sentinel-s2-l2a-cogs", "sentinel-s2-l2a*"],
    }

The following example is used to choose a list of specific items from the collection identified as `sentinel-s2-l2a-cogs`.

.. code-block:: javascript

    {
        "table": "item",
        "colleciton-id": "sentinel-s2-l2a-cogs",
        "item-id": ["S2A_MSIL2A_20200101T105031_N0213_R051_T31UFS_20200101T124921", "S2A_MSIL2A_20200101T105031_N0213_R051_T31UFS_20200101T124921*"],
    }

The following example is used to choose a specific item from the collection identified as `sentinel-s2-l2a-cogs` and contained `sentinel-s2-l2a`.

.. code-block:: javascript

    {
        "table": "item",
        "colleciton-id": ["sentinel-s2-l2a-cogs", "sentinel-s2-l2a*"],
        "item-id": "S2A_MSIL2A_20200101T105031_N0213_R051_T31UFS_20200101T124921",
    }
