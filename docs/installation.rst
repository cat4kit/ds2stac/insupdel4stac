..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0


==============
Instalation
==============

.. warning:: This page has been automatically generated as has not yet been reviewed
   by the authors of insupdel4stac!


To install the *insupdel4stac* package, we recommend that you install it from
PyPi via

.. code:: bash

   pip install insupdel4stac

Or install it directly from `the source code repository on
Gitlab <https://codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac>`__
via:

.. code:: bash

   pip install git+https://codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac.git

The latter should however only be done if you want to access the
development versions.


.. _install-develop:
Installation for development
============================

Please head over to our :ref:`contributing` for
installation instruction for development.
