..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0


..
   ds2stac documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. # define a hard line break for HTML
.. |br| raw:: html

   <br />

=============
INSUPDEL4STAC
=============

**A Python package to provide seamless functionality for INSerting, UPDating, and DELeting STAC-Metadata toward either pgSTAC or STAC-API.**


.. image:: https://codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac/badges/main/pipeline.svg
   :target: https://codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac/-/pipelines?page=1&scope=all&ref=main
.. image:: https://img.shields.io/pypi/v/insupdel4stac.svg
   :target: https://pypi.python.org/pypi/insupdel4stac/
.. image:: https://readthedocs.org/projects/insupdel4stac/badge/?version=latest
   :target: https://insupdel4stac.readthedocs.io/en/latest/
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
.. image:: https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336
   :target: https://pycqa.github.io/isort/
.. image:: https://img.shields.io/badge/code%20style-pep8-orange.svg
   :target: https://www.python.org/dev/peps/pep-0008/
.. image:: http://www.mypy-lang.org/static/mypy_badge.svg
   :target: http://mypy-lang.org/
.. image:: https://codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac/badges/main/coverage.svg
   :target: https://codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac/-/graphs/main/charts
.. image:: https://api.reuse.software/badge/codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac
   :target: https://api.reuse.software/info/codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac


Overview:
=================

STAC specification is a method of exposing spatial and temporal data collections in a standardized manner. Specifically, the `SpatioTemporal Asset Catalog (STAC) <https://stacspec.org/en>`_ specification describes and catalogs spatiotemporal assets using a common structure.
This package is desigend to manage ingestion, updating and and removing of STAC-Collections and STAC-Items toward either `PostgreSQL schema and functions for STAC (pgSTAC) <https://stac-utils.github.io/pgstac/>`_  or `STAC compliant FastAPI application (STAC-FASTAPI) <https://stac-utils.github.io/stac-fastapi/>`_  services.


Installation
=================
You can find installation instructions in the [Installation](installation) section.

.. toctree::
   :maxdepth: 2

   installation

Tutorials
=========================
A comprehensive tutorial designed to enhance your understanding of the functionality of this project.

.. toctree::
   :maxdepth: 2

   tutorials/index

Examples
=========================
A bunch of examples is provided in this section.

.. toctree::
   :maxdepth: 2

   examples/index


API Reference
=========================
This API reference is auto-generated from the Python docstrings. The table of contents on the left is organized by module.

.. toctree::
   :maxdepth: 2

   api


Contribution
=========================


.. toctree::
   :maxdepth: 2

   contributing
