# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: EUPL-1.2
pypgstac==0.7.10
psycopg==3.1.10
psycopg_pool==3.1.7
pystac==1.8.3
requests==2.26.0
versioneer==0.29
